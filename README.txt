Project Structure:
==================
parallelcomputing/
├── assignment1
│   ├── average_matrix
│   │   ├── build
│   │   │   └── average_matrix
│   │   ├── Makefile
│   │   ├── src
│   │   │   └── average_matrix.c
│   │   └── tests
│   │       ├── test_avarage_its-cs190.txt
│   │       └── test_average_localhost.txt
│   └── pi
│       ├── build
│       │   └── pi
│       ├── Makefile
│       ├── src
│       │   ├── pi.c
│       └── tests
│           └── test_pi_its-cs190.txt
└── README


Compile with make:
==================
(WARNING) Make file uses Intel C Compiler by default.

$ cd parallelcomputing/assignment1/pi
$ make
$ ./build/pi <N> <Thread count>

$ cd parallelcomputing/assignment1/average_matrix
$ make
$ ./build/average_matrix <N> <thread_count> <ineration_count>


Manual compilation:
===================
icc -O2 -openmp -std=c99 -o bsp1 bsp1.c -lm


Tests and speadups:
===================
You can find the tests and speedup values under:

(on cluster) parallelcomputing/assignment1/average_matrix/tests/test_avarage_its-cs190.txt
(on localhost) parallelcomputing/assignment1/average_matrix/tests/test_average_localhost.txt