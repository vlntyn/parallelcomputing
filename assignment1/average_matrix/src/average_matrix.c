/*
 ============================================================================
 Name        : average_matrix.c
 Author      : Moritz Wilde, Valenty Kukushkin
 Version     : 1.0
 Copyright   : 
 Description :
 ============================================================================
 */
#include <stdio.h>
#include <stdlib.h>
#include <float.h>
#include <math.h>
#include <omp.h>

#define DISTANCE 2
#define COUNT 25

// global variable matrix
double *outputBlock;
double **outputMatrix;



void printMatrix(double ** mtrx, int n) {
    
    for (int i = 0; i < n; ++i) {
        for (int j = 0; j < n; ++j) {
            printf("\t%0.4lf ", mtrx[i][j]);
        }
        printf("\n\n");
    }
}


//
// Computes average for specified matrix-index
// =============================================================================
double average(double ** mtrx, int x, int y, int n) {

    int startX = x - DISTANCE;
    int startY = y - DISTANCE;

    int endX = x + DISTANCE;
    int endY = y + DISTANCE;

    double sum = 0;

    for (int currentX = startX; currentX <= endX; currentX++) {
        for (int currentY = startY; currentY <= endY; currentY++) {

            int reversedX = currentX, reversedY = currentY;

            if (currentX < 0) {
                reversedX = -currentX - 1;
            }
            if (currentY < 0) {
                reversedY = -currentY - 1;
            }
            if (endX >= n) {
                reversedX = 2 * n - endX - 1;
            }
            if (endY >= n) {
                reversedY = 2 * n - endY - 1;
            }

            
            sum += mtrx[reversedX][reversedY];
        }
    }
    

    return sum;
}

void matrixIteration(double ** mtrx, int n) {
    
    double result;
    
    // parallel
    #pragma omp parallel private(result) 
    {
    #pragma omp for
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < n; j++) {        
            result = average(mtrx, i, j, n);        
            outputMatrix[i][j] = result / COUNT;
        }
    }
    }
}




int main(int argc, char *argv[]) {

    double startTime, endTime;
    
    
    if (argc != 4) {
        printf("Usage: ./average_matrix <N> <Threadcount> <iteration_count>\n");
        return 1;
    }

    // read command line parameter
    int n = atoi(argv[1]);
    int threads = atoi(argv[2]);
    int iteration_count = atoi(argv[3]);

    printf("Welcome to average_matrix!\n");
    printf("N = %d\nThreadcount = %d\nIteration count = %d\n", n, threads, iteration_count);
    printf("Workin'...\n");

    
    // application timer starts here
    startTime = omp_get_wtime();

    
    
    //
    // create initial matrix
    // =========================================================================
    double *initBlock = malloc(sizeof (double)*n * n);
    double **initMatrix = malloc(sizeof (double)*n);

    for (int i = 0; i < n; i++) {
        initMatrix[i] = initBlock + n*i;
    }
    
    // initialize the matrix
    // this code should not be paralell computed
    for (int i = 0; i < n; i++) {
        for (int j = 0; j < n; j++) {
            initMatrix[i][j] = (2 * i + 3 * j) % (2 * n);
        }
    }
    
    
    
    //
    // start of parallel section
    // =========================================================================
    // set number of threads
    omp_set_num_threads(threads);

    
    
    //
    // create and output matrix
    // =========================================================================
    outputBlock = malloc(sizeof (double)*n * n);
    outputMatrix = malloc(sizeof (double)*n);
    for (int i = 0; i < n; i++) {
        outputMatrix[i] = outputBlock + n*i;
    }

    
    //
    // iteration count
    // =========================================================================
    int i = 0;
    while (i < iteration_count) {
        if (i == 0) {
            matrixIteration(initMatrix, n);
        } else {
            matrixIteration(outputMatrix, n);
        }
        i++;
    }

    

    //
    // find min and max element
    // =========================================================================
    double min = DBL_MAX;
    double max = DBL_MIN;
    double loc_max, loc_min;
    
 
    for (int i = 0; i < n; i++) {
#pragma omp parallel private(loc_max, loc_min)
        {
            loc_max = DBL_MIN;
            loc_min = DBL_MAX;
            
#pragma omp for schedule(static, 1)
            for (int j = 0; j < n; j++) {
                loc_min = fmin(loc_min, outputBlock[i*j]);
                loc_max = fmax(loc_max, outputBlock[i*j]);
            }
            
#pragma omp critical
            {
            min = fmin(min, loc_min);
            max = fmax(max, loc_max);
            }
        }
        
    }
    
    
    
    // alternative: find min and max element
    // this code also very efficient
    // =========================================================================
    /*
    double min = DBL_MAX;
    double max = DBL_MIN;
    
    // stable runtime, better then if else (2x flops pro element))
    for (int i = 0; i < n*n; i++) {
        
        double current = outputBlock[i];
        min = fmin(min, current);
        max = fmax(max, current);
    }
    */
    
    
    // application timer ends here
    endTime = omp_get_wtime();
    

    printf("Ready! Time: %lfs\n", (endTime - startTime));
    
    
    
    if (n < 20) {
        printMatrix(outputMatrix, n);
    }
    
    printf("MIN-Value:\t %0.4lf\n", min);
    printf("MAX-Value:\t %0.4lf\n", max);
    printf("First element:\t %0.4lf\n", outputMatrix[0][0]);
    

    return 0;
}
