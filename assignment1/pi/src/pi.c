#include <stdio.h>
#include <math.h>
#include <stdlib.h>
#include <omp.h>

// function declaration
double CalcPi(int n);

double startTime, stopTime;

int main(int argc, char **argv)
{

	if (argc != 3)
	{
		// error if no args provided
		printf("Usage: ./pi N Threadcount\n");
		printf(" e.g.: ./pi 150000000 4\n");
		return 1;
	}

	printf("Welcome to pi!\n");
	printf("Program is running...");
	const double fPi25DT = 3.141592653589793238462643;
	// number of intervals
	int n = atoi(argv[1]);
	// number of threads
	int threads = atoi(argv[2]);

	// set the number of threads
	omp_set_num_threads(threads);

	double fPi;

	if (n <= 0 || n > 2147483647)
	{
		// error, if n is not in range
		printf("\ngiven value has to be between 0 and 2147483647\n");
		return 1;
	}

	// Execution only in the master thread
#pragma omp master
	{
		startTime = omp_get_wtime();
	}
	//
	fPi = CalcPi(n);

	// Execution only in the master thread
#pragma omp master
	{
		stopTime = omp_get_wtime();
	}

	printf("\npi is approximately = %.20f \nError               = %.20f\n", fPi, fabs(fPi - fPi25DT));

	printf("Ready! Time: %f\n", stopTime - startTime);
	return 0;
}

//
double f(double a)
{
	return (4.0 / (1.0 + a * a));
}

//
double CalcPi(int n)
{

	double fX;
	// sizeof each interval
	const double fH = 1.0 / (double) n;
	double fSum = 0.0;
	int i;

	// Work-Sharing
	// Creation of the setted number of threads but they still don't do any work
#pragma omp parallel private(fX)
	{
		// The created threads begin there work
#pragma omp for reduction(+ : fSum)
		for (i = 0; i < n; i += 1)
		{
			fX = fH * ((double) i + 0.5);
			fSum += f(fX);
		}
	}
	// sum_i[0..n] 4/(1 + (i + 1/2)/n)

	return fH * fSum;
}
