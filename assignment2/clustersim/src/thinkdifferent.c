/* 
 * File:   thinkdifferent.c
 * Author: kukushkin
 *
 * Created on February 2, 2014, 12:41 AM
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <omp.h>
#include <string.h>
#include <float.h>
#include <math.h>

#define REGULAR 2
#define FPGA 1
#define TESLA 0

#define MAX_INDEX 7
#define MAX_THREADS 8

                // { visited, type, threads, (time)}
//char queue[5][4] = {{1, 2, 4, 20}, {1, 2, 2, 4}, {1, 1, 1, 5}, {1, 0, 1, 3}, {1, 0, 1, 4}};
char queue[7][5];

void process(char *c, int t, int time, int a) {
    printf("T%d A%d started with %s\n", t, a, c);
    sleep(time);
}


char* substring(const char* str, size_t begin, size_t len) {
    if (str == 0 || strlen(str) == 0 || strlen(str) < begin || strlen(str) < (begin + len)) {
        return 0;
    }

    return strndup(str + begin, len);
}

void parseFile() {
		//
	    // Read file and create queue
	    // =========================================================================
	    FILE *file = fopen("src/resource.txt", "r");
	    char *line = NULL;
	    //
	    size_t len = 7;
	    size_t read;

	    if (file) {
	    	int row = 0;
	        while ((read = getline(&line, &len, file)) != EOF) {
//	            printf("LINE:%s\n", line);
	            char *tok = NULL;
	            tok = strtok(line, " ");
	            int column = 0;
				char* letter;
				char* number;
				size_t begin = 0;
				size_t end = 1;
	            // read 3 tokens
	            while (column <= 3) {
	                int tokenSize = strlen(tok);
	                //if token has 2 symbols on position 1
	                if ((column == 1) && (tokenSize > 1)) {
	                    number = substring(tok, begin, end);
	                    letter = substring(tok, begin + 1, end);
	                    int n = atoi(number);
	                    int l;
	                    if (*letter == 'T') {
	                        l = TESLA;
	                    } else if (*letter == 'F') {
	                        l = FPGA;
	                    }
	                    queue[row][column] = n;
	                    column++;
	                    queue[row][column] = l;
	                //if token has just one symbol on position 1
	                } else if ((column == 1) && (tokenSize < 2)) {
	                    int n = atoi(tok);
	                    queue[row][column] = n;
	                    column++;
	                    queue[row][column] = REGULAR;
	                }
	                else {
	                    int n = atoi(tok);
	                    queue[row][column] = n;
	                }
	                tok = strtok(NULL, " ");
	                column++;
	            }
	            //set flag for task working status - default = 1
	            queue[row][4] = 1;
	            row++;
	        }
	        fclose(file);
	    }
	    if (line) {
	        free(line);
	    }
	    printf("Input from file:\n");
	    for (int j = 0; j < 7; j++) {
	    	for(int k = 0; k < 4; k++) {
	    		printf("%d ", queue[j][k]);
	    	}
	    	printf("\n");
	    }
}


int main(int argc, char** argv) {
    
    int index = 0;
    int thIndex = 0;
    int qLength = 7;
    parseFile();
    
    printf("Welcome to Cluster Simulation! \n");
    while (qLength > 0) {
        while (index < MAX_INDEX) {
            //
            if (queue[index][4] == 1) {
//            printf("Task nr: %d\n", index+1);
                
                // get task from queue
                int hwType = queue[index][2];
                int thNeeded = queue[index][1];
                int workingTime = queue[index][3];
                
                #pragma omp parallel num_threads(MAX_THREADS) // team of threads
                {
                    if (hwType == REGULAR) {
                        
                        #pragma omp single
                        while (thIndex < thNeeded) { //MAX_THREADS
                        	int splittedWorkingTime = workingTime / thNeeded;
                            
                            #pragma omp task
                            {
                                process("REGULAR", omp_get_thread_num(), splittedWorkingTime, index);
                                if(queue[index][4] == 1) {
									#pragma omp atomic write
                                	queue[index][4] = 0;
                                }
                            }
                            thIndex++;
                        }
                        #pragma omp barrier
                        
                    } else if (hwType == FPGA) {
                        
                        #pragma omp single
                        while (thIndex < thNeeded) {
                            
                            #pragma omp task
                            {
                            	if(omp_get_thread_num() == 1 || omp_get_thread_num() == 2) {
                            		process("FPGA", omp_get_thread_num(), workingTime, index);
                            		if(queue[index][4] == 1) {

                            			#pragma omp atomic write
                            			queue[index][4] = 0;
                            		}
                            	}
                            }
                            thIndex++;
                        }
                       #pragma omp barrier

                    } else if (hwType == TESLA) {
                        
                        #pragma omp single
                        while (thIndex < thNeeded) {
                            
                            #pragma omp task
                            {
                                if(omp_get_thread_num() == 0) {
                                	process("TESLA", omp_get_thread_num(), workingTime, index);
                                	if(queue[index][4] == 1) {
										#pragma omp atomic write
                                		queue[index][4] = 0;
                                	}
                                }
                            }
                            thIndex++;
                        }
                       #pragma omp barrier
                    }
                }

            } else {
                // this task has already been processed
//                printf("already processed\n");
            }
            index++;
            thIndex = 0;
        } // while tasks in queue
        qLength--;
        index = 0;
	} // first wile
    printf("Done!\n");

    return (EXIT_SUCCESS);
}
