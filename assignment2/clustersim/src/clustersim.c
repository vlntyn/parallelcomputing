/*
 ============================================================================
 Name        : clustersim.c
 Author      : Moritz Wilde, Valenty Kukushkin
 Version     : 1.0
 Copyright   : 
 Description : Computer-Cluster Simulator
 ============================================================================
 */
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <float.h>
#include <math.h>
#include <omp.h>

#define MAX_THREADS 8

void sleep(int workTime) {
    double startZeit = omp_get_wtime();
    double jetzt = startZeit;
    while (jetzt - startZeit < workTime)
        jetzt = omp_get_wtime();
}

char* substring(const char* str, size_t begin, size_t len) {
    if (str == 0 || strlen(str) == 0 || strlen(str) < begin || strlen(str) < (begin + len)) {
        return 0;
    }

    return strndup(str + begin, len);
}

int main(int argc, char**argv) {

    // Prints arguments
    printf("Welcome to Cluster Simulator!\n");

    struct task {
        int number;
        int type;
        int time;
    };

    //
    // Read file and create queue
    // =========================================================================
    FILE *file = fopen("src/resource.txt", "r");
    char *line = NULL;
    size_t len = 5;
    size_t read;

    if (file) {
        while (read = getline(&line, &len, file) != EOF) {
            int taskParts[4];
            printf("LINE:%s\n", line);
            char *tok = NULL;
            tok = strtok(line, " ");
            int i = 0;
            while (i <= 3) {
                size_t begin = 0;
                size_t end = 1;
                char* letter;
                char* number;
                int tokenSize = strlen(tok);
                if ((i == 1) && (tokenSize > 1)) {
                    number = substring(tok, begin, end);
                    letter = substring(tok, begin + 1, end);
//                    if (letter == NULL) {
//                        *letter = '0';
//                    }
                    int n = atoi(number);
                    int l;
                    if (*letter == 'T') {
                        l = 1;
                    } else if (*letter == 'F') {
                        l = 2;
                    }
                    taskParts[i] = n;
                    i++;
                    taskParts[i] = l;
                } else if ((i == 1) && (tokenSize < 2)) {
                    int n = atoi(tok);
                    taskParts[i] = n;
                    i++;
                    taskParts[i] = 0;
                } else {
                    int n = atoi(tok);
                    taskParts[i] = n;
                }
                tok = strtok(NULL, " ");
                i++;
            }
            for (int j = 0; j < (sizeof (taskParts) / sizeof (int)); j++) {
                printf("ArrayParts %d: %d\n", j, taskParts[j]);
            }
        }
        fclose(file);
    }
    if (line) {
        free(line);
    }
    return (EXIT_SUCCESS);
}
