/*
 ============================================================================
 Name        : clustersim.c
 Author      : Moritz Wilde, Valenty Kukushkin
 Version     : 1.0
 Copyright   : 
 Description : Computer-Cluster Simulator
 ============================================================================
 */
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <float.h>
#include <math.h>
#include <omp.h>
#include <unistd.h>

#define MAX_THREADS 8
#define MAX_INDEX 7

#define TESLA 10
#define FPGA 20
#define NORMAL 5

int taskCount = 5;
//int taskQueue[7][5] = {
//    {1, 1, 1, 5, 7},
//    {1, 2, 1, 0, 5},
//    {1, 3, 4, 1, 8},
//    {1, 4, 4, 1, 12},
//    {1, 5, 3, 1, 9},
//    {1, 6, 2, 5, 6},
//    {1, 7, 4, 5, 8}
//};
int taskQueue[MAX_INDEX][5] = {
    {1, 0, 1, 5, 20},
    {1, 1, 1, 0, 4},
    {1, 2, 1, 1, 9},
    {1, 3, 1, 1, 2},
    {1, 4, 1, 1, 7},
    {1, 5, 1, 5, 10},
    {1, 6, 1, 5, 15}
};

void start(int n);
int get_next_task();
void doJob(int dauer, int n, int taskNumber);

int main(int argc, char**argv) {

    // Prints arguments
    printf("Welcome to Cluster Simulator!\n\n");

    //
    // Read file and create queue
    // =========================================================================
    //    FILE *file = fopen("src/resource.txt", "r");
    //    char *line = NULL;
    //    size_t len = 5;
    //    size_t read;
    //
    //    if (file) {
    //        while ((read = getline(&line, &len, file)) != EOF) {
    //            printf("line: %s\n", line);
    //            //
    //            char *tok = NULL;
    //            tok = strtok(line, " ");
    //            for (int i = 0; i < 3; i++) {
    //                printf("Token: %s\n", tok);
    //                tok = strtok(NULL, " ");
    //            }
    //        }
    //        if (line) {
    //            free(line);
    //        }
    //        fclose(file);
    //    }

    omp_set_num_threads(MAX_THREADS);

    //
    // Start every node in cluster
    // =========================================================================
#pragma omp parallel shared(taskQueue)
    {
#pragma omp for  schedule(static, 1) 
        for (int i = 0; i < MAX_THREADS; i++) {
            start(omp_get_thread_num());
        }
    }
    printf("Warteschlange ist leer. Programm beendet.\n");

    // EXIT APP
    return 0;
}


//void process(int thread_num) {
//    printf("I am thread %d.\n", thread_num);
//    
//    for (int i = 0; i < taskCount; i++) {
//        if (taskQueue[i][1]) {
//            // type 
//            if (taskQueue[i][3] == NORMAL) {
//                printf("\t(mit Tesla)\n");
//                
//            } else if (taskQueue[i][3] == ) {
//                
//            }
//            printf("\tT%d A%d gestartet.\n", thread_num, i);
//            taskQueue[i][1] = 0;
//            break;
//        } else {
//            printf("\tT%d A%d skip.\n", thread_num, i);
//        }
//    }
//    printf("T%d beendet.\n\n", thread_num);
//}

//int computer[8];
int availableComputers;

void start(int n) {
    availableComputers++;

    int currentTask = 0;
    

    while (taskQueue[currentTask][0] == 1) {
        printf("taskQueue[currentTask][0] == %d\n", taskQueue[currentTask][0]);
        currentTask = get_next_task();
        printf("CURRENT TASK - INDEX: %d\n", currentTask);
        if (currentTask < MAX_INDEX) {
            int neededResources = taskQueue[currentTask][2];
            printf("CURRENT TASK - NEEDED RESOURCES: %d\n", neededResources);
            int workingTime = taskQueue[currentTask][4];
            printf("CURRENT TASK - WORKING TIME: %d\n", workingTime);
            if (neededResources == 1) {
                availableComputers--;
                doJob(workingTime, n, currentTask);
                availableComputers++;
                printf("\n");
                taskQueue[currentTask][0] = 0;
            } else if (availableComputers >= neededResources) {
#pragma omp for schedule(static, 1)
                for (int i = 0; i < neededResources; i++) {
                    printf("int i: %d\n", i);
                    int avrgTimePerThread = workingTime / neededResources;
                    availableComputers--;
                    doJob(avrgTimePerThread, n, currentTask);
                    availableComputers++;
                    printf("\n");
                }
            } else {
                availableComputers++;
                printf("not enough ressources\n");
            }
        }
    }
}

int index = 0;

int get_next_task() {
    int tmp;
#pragma omp critical
    {
        tmp = index;
        if (index > MAX_INDEX) {
            tmp = -1;
        } else {
            index++;
        }
    }
    return tmp;
}

void doJob(int dauer, int n, int taskNumber) {
    printf("THREAD %d is working on Task %d and needs %d s\n", n, taskNumber, dauer);
    sleep(dauer);
    printf("THREAD %d is coming back from Task %d\n", n, taskNumber);


}
