/*
 ===============================================================================
 Name        : factor.c
 Author      : Moritz Wilde, Valenty Kukushkin
 Version     : 1.0
 Copyright   :
 Description : Factorization of numbers
 ===============================================================================
 */
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>
#include <math.h>
#include <omp.h>

/**
 *
 */
int main(int argc, char** argv) {

    double startTime, endTime;

    if (argc != 3 || atoi(argv[2]) < 2) {
        printf("Usage: ./factor <Threads Count> <N>\n");
        return 1;
    }

    // print greetings
    printf("Welcome to factor!!!\n");
    printf("Threads count: %i\n", atoi(argv[1]));
    printf("N: %d\n", atoi(argv[2]));

    // read CL parameters
    int threads = atoi(argv[1]);
    int n = atoi(argv[2]);
    int primesToCheck = sqrt(n);
    
    // debug
    // printf("sqrt=%d \n", sieveSize);

    // set number of threads
    omp_set_num_threads(threads);

    int * const nNumbers = malloc(sizeof (int) * n+1);

    // application timer starts here
    startTime = omp_get_wtime();

    printf("working...\n");
    
    // initialize the array of size (n + 1)
#pragma omp parallel for
    for (int i = 0; i < n+1; i++) {
        nNumbers[i] = 1;
    }
    
    
    //
    // Sieve of Eratosthenes
    // =========================================================================
#pragma omp parallel for schedule(dynamic)
    for (int i = 2; i <= primesToCheck; i++) {
        if (nNumbers[i]) {
            for (int j = i * i; j < n+1; j += i) {
                nNumbers[j] = 0;
            }
        }
    }
    
    long count = 0;
    
    //
    // Calculating the sum of prime-factors
    // =========================================================================
#pragma omp parallel for reduction(+ : count)
    for (int i = 2; i < n+1; i++) {

        int local_count = 0;
        if (nNumbers[i]) {

            int tmp = n;
            while (tmp >= i) {
                tmp /= i;
                local_count += tmp;
            }
        }
        count += local_count;
    }

    // application timer ends here
    endTime = omp_get_wtime();

    printf("Ready! Time: %lf s\n", (endTime - startTime));
    printf("Count: %ld \n", count);

    return (EXIT_SUCCESS);
}
